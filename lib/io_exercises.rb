# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game

  comp_choose = rand(1..100)

  user_guesses = 0

  while true
    puts "Please guess a number from 1 to 100...!"
    user_guess = gets.chomp.to_i
    user_guesses += 1

    case user_guess <=> comp_choose
    when -1
      puts "Your guess of #{user_guess} is too low...try again!"
    when 0
      puts "Well done, #{user_guess} was the number I was thinking of!"
      break
    when 1
      puts "Your guess of #{user_guess} is too high...try again!"
    end
  end
  puts "You guessed #{user_guesses} times"
end

if __FILE__ == $PROGRAM_NAME
  guessing_game
end
